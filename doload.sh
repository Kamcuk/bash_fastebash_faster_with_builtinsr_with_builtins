#!/bin/bash

runv() {
	echo "+ $*" >&2
	"$@"
}

make all
for i in "$(dirname "$0")"/_build/loadables/*; do
	runv enable -f "$i" "$(basename "$i")"
done

