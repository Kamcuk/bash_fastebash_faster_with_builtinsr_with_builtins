export CMAKE_GENERATOR = Ninja
all:
	cmake -S. -B_build -DCMAKE_BUILD_TYPE=Release
	cmake --build _build
clean:
	rm -rf _build
