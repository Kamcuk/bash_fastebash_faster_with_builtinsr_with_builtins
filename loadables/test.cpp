#include "bash_builtin.h"
#include <vector>
#include <iostream>
#include <exception>

static int mytest_builtin(WORD_LIST *list)
{
	try {
		std::vector<int> a{1, 2, 3, 4, 5};
		for (auto&& i : a) {
			std::cout << i << " ";
		}
		std::cout << "\n";
	} catch(const std::exception& e) {
		std::cout << e.what() << std::endl;
	  	return EXECUTION_FAILURE;
	} catch(...) {
	  	return EXECUTION_FAILURE;
	}
 	return EXECUTION_SUCCESS;
}

static const char *const mytest_doc[] = {
	(const char*)"mytest", 0
};

extern "C" struct builtin mytest_struct;
struct builtin mytest_struct = {
	(char*)"mytest",			/* builtin name */
	mytest_builtin,		/* function implementing the builtin */
	BUILTIN_ENABLED,	/* initial flags for builtin */
	(char**)mytest_doc,		/* array of long documentation strings. */
	"mytest ?",	/* usage synopsis; becomes short_doc */
	0			/* reserved for internal use */
};

