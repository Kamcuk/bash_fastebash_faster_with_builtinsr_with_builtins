#include <config.h>
#if defined (HAVE_UNISTD_H)
#include <unistd.h>
#endif
#include <stdio.h>
#include <sys/types.h>
#include "posixstat.h"
#include <stdio.h>
#include <pwd.h>
#include <grp.h>
#include <errno.h>
#include "posixtime.h"
#include "bashansi.h"
#include "shell.h"
#include "builtins.h"
#include "common.h"
#include "bashgetopt.h"

static int geterrno_builtin(WORD_LIST *list) {
	printf("%d\n", errno);
	return (EXECUTION_SUCCESS);
}

static char *doc[] = {
	"geterrno: prints errno",
	NULL
};

struct builtin geterrno_struct = {
	"geterrno",
	geterrno_builtin,
	BUILTIN_ENABLED,
	doc,
	"getrrno: prints errno",
	0,
};

